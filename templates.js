module.exports = {
  building: {
    HOUSE: {
      motif: ' H ',
      width: 2,
      height: 2
    },
    FORGE: {
      motif: ' F ',
      width: 4,
      height: 3
    },
    BARRACK: {
      motif: ' C ',
      width: 5,
      height: 5
    },
    FARM: {
      motif: '###',
      width: 4,
      height: 4
    },
    FOUNTAIN: {
      motif: '(O)',
      width: 1,
      height: 1
    },
    GATE_H: {
      model: ["[  ", "   ", "   ", "  ]"]
    },
    GATE_V: {
      model: ["MMM", "   ", "   ", "WWW"]
    },
    TOWER: {
      motif: ' T ',
      width: 3,
      height: 3
    },
    MILL: {
      motif: '** ',
      width: 4,
      height: 4
    },
    RIVER_SOUTH_WEST: {
      motif: ' ~ ',
      width: 12,
      height: 2
    },
    CASTLE: {
      motif: '|^|',
      width: 7,
      height: 6
    },
    CHURCH: {
      motif: ' + ',
      width: 4,
      height: 5
    },
    RIVER_HARBOR: {
      motif: '~~~',
      width: 8,
      height: 4
    },
    PONTOON_PRINC: {
      motif: '||)',
      width: 1,
      height: 33
    },
    PONTOON_BOARDING: {
      motif: '=======',
      width: 1,
      height: 1
    },
    SHIP_TOP: {
      motif: '  --/--/--- ',
      width: 1,
      height: 1
    },
    SHIP_MID: {
      motif: ' <=========)',
      width: 1,
      height: 1
    },
    SHIP_BOT: {
      motif: '  --\\--\\--- ',
      width: 1,
      height: 1
    },
    SHIP_EMPTY: {
      motif: '            ',
      width: 1,
      height: 1
    },
	WONDER: {
      motif: ' ♥ ',
      width: 13,
      height: 7
	},
    STABLE: {
      motif: ' S ',
      width: 4,
      height: 4
    },
    ARCHERY_CAMP_V: {
      motif: ' A ',
      width: 3,
      height: 5
    },
    ARCHERY_CAMP_H: {
      motif: ' A ',
      width: 5,
      height: 3
    },
    SIEGE_WORKSHOP: {
      motif: ' I ',
      width: 4,
      height: 4,
    },
    TOWER_BOMBARD: {
      motif: 'TB ',
      width: 3,
      height: 3
    }
  },
  wall: {
    HORIZONTAL: {
      SMALL: {
        motif: '---'
      },
      LARGE: {
        motif: '==='
      },
      REINFORCED:{
        motif: '≣≣≣'
      }
    },
    VERTICAL: {
      SMALL: {
        motif: ' | '
      },
      LARGE: {
        motif: '|||'
      },
      REINFORCED:{
        motif: '|||||'
      }
    }
  }
};
